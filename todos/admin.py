from django.contrib import admin
from todos.models import TodoList, TodoItem


# Register your models here.
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "created_on",
    )


@admin.register(TodoItem)
class TodoItemAdim(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
        "is_completed",
    )
