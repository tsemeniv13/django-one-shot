from django.shortcuts import render, get_object_or_404
from todos.models import TodoList


def to_do_list(request):
    todolist = TodoList.objects.all()
    context = {
        "to_do_list": todolist,
    }
    return render(request, "todos/to_do_list.html", context)


def show_to_do_list_details(request, id):
    todo_list_details = get_object_or_404(TodoList, id=id)
    context = {"todo_list_detail": todo_list_details}
    return render(request, "todos/")
