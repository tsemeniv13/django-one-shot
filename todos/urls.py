from django.urls import path
from todos.views import to_do_list


urlpatterns = [path("", to_do_list, name="todo_list_list")]
